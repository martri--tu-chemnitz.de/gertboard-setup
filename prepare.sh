if (($# < 1))
then
    echo "Not enough arguments! Please add host name!"
elif (($# > 1))
then
    echo "Too many arguments! Please only add host name!"
else
    echo "Set new hostname."
    echo $1 > '/etc/hostname'
fi

apt-get update

echo "Installing vim"
apt-get install -y vim

echo "Installing gedit"
apt-get install -y gedit

echo "Installing avr-gcc"
apt-get install -y avr-libc gcc-avr arduino

echo "Installing doxygen"
apt-get install -y doxygen

echo "Installing git"
apt-get install -y git

echo "Enabling ssh"
sudo systemctl enable ssh
sudo systemctl start ssh

echo "Installing minicom"
apt-get install -y minicom screen
wget https://gitlab.hrz.tu-chemnitz.de/martri--tu-chemnitz.de/gertboard-setup/-/raw/main/minirc.ama0 -O /etc/minicom/minirc.ama0
sudo adduser pi tty

cd /tmp
echo "Installing modified AVRdude"
sudo apt-get install libncurses5
wget https://gitlab.hrz.tu-chemnitz.de/martri--tu-chemnitz.de/gertboard-setup/-/raw/main/avrdude_5.10-4_armhf.deb
dpkg -i avrdude_5.10-4_armhf.deb
sudo apt-get install -fy
chmod 4755 /usr/bin/avrdude

echo "Adding programmers and boards"
cd /tmp
wget https://gitlab.hrz.tu-chemnitz.de/martri--tu-chemnitz.de/gertboard-setup/-/raw/main/boards.txt
wget https://gitlab.hrz.tu-chemnitz.de/martri--tu-chemnitz.de/gertboard-setup/-/raw/main/programmers.txt
cd /usr/share/arduino/hardware/arduino
sudo mv boards.txt board.txt.bak
sudo mv /tmp/boards.txt .
sudo mv programmers.txt programmers.txt.bak
sudo mv /tmp/programmers.txt .

echo "Downloading AVRSetup"
cd /tmp
wget https://gitlab.hrz.tu-chemnitz.de/martri--tu-chemnitz.de/gertboard-setup/-/raw/main/avrsetup
sudo mv /tmp/avrsetup /usr/local/bin
sudo chmod 755 /usr/local/bin/avrsetup

echo "Disable Bluetooth for minicom to work:"
sudo systemctl disable hciuart

echo "Disable bluetooth device tree overlay for minicom to work:"
cd /boot
if [ -f config.bak ]; then
  echo "Backup exists: not overwriting"
else
  cp -a config.txt config.bak
  sudo echo "dtoverlay=disable-bt" >> /boot/config.txt
  echo "OK"
fi

cd /boot
echo -n "cmdline.txt: "
if [ -f cmdline.txt.bak ]; then
  echo "Backup exists: not overwriting"
else
  cp -a cmdline.txt cmdline.txt.bak
  cat cmdline.txt					|	\
		sed -e 's/console=ttyAMA0,115200//'	|	\
		sed -e 's/console=tty1//'		|	\
    sed -e 's/console=serial0,115200//' | \
		sed -e 's/kgdboc=ttyAMA0,115200//' > /tmp/cmdline.txt.$$
  mv /tmp/cmdline.txt.$$ cmdline.txt
  echo "OK"
fi

echo "Initialising the ATMega on Gertboard"
echo "1" | avrsetup

echo "All Done."
echo "Check and reboot now to apply changes."
exit 0
